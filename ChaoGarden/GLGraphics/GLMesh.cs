﻿using OpenTK;
using OpenTK.Graphics;
using System.Collections.Generic;
using System;
using System.IO;
using Assimp;
using OpenTK.Graphics.OpenGL;
using ChaoGarden;
using System.Runtime.InteropServices;

namespace ChaoGarden.GLGraphics
{
    public struct MeshNode
    {
        public string name;
        public int numChildren;
        public int numMeshes;
        public MeshNode[] children;
        public int[] meshes;
        public Matrix4 transformation;
    }

    public struct MeshPart
    {
        public int numFaces;
        public int numVertices;
        public int indexFaces;
        public int indexVertices;
        public int indexMaterial;
        public int baseVertex;
    }

    public struct BoneInfo
    {
        public Matrix4 offsetMatrix;
    }

    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct BoneData
    {
        public fixed Int32 boneIDs[4];
        public fixed float boneWeights[4];
    }

    public struct AnimationKey
    {
        public int time;
        public Vector3 value;
    }

    public struct AnimationRotationKey
    {
        public int time;
        public OpenTK.Quaternion value;
    }

    public struct AnimationNode
    {
        public bool keyFramePerTick;
        public List<AnimationKey> positionKeys;
        public List<AnimationKey> scalingKeys;
        public List<AnimationRotationKey> rotationKeys;
        public List<Matrix4> transforms;
    }

    public struct Animation
    {
        public int duration;
        public int ticksPerSecond;
        public Dictionary<string, AnimationNode> nodes;
    }

    public struct Material
    {
        public GLTexture textureDiffuse;
        public GLShader shader;
    }

    public class GLMesh : IResourceFlagsInterface
    {
        public int numBones = 0;
        public Dictionary<string, int> boneMapping = new Dictionary<string, int>();
        public List<BoneInfo> bones = new List<BoneInfo>();
        public Dictionary<string, Animation> animations = new Dictionary<string, Animation>();
        public MeshNode rootNode;

        const int MAX_BONE_WEIGHTS = 4;
        private string filename;
        private string dirname;

        private int numMeshParts;
        private int numMaterials;
        private MeshPart[] meshParts;
        private Material[] materials;
        private Single[] vertexElementsData;
        private uint[] facesData;

        private int vertexArrayObject;
        private int vertexElementsBuffer;
        private int facesBuffer;
        private int indexVBO = 0;
        private int indexIBO = 0;

        private bool hasNormals = false;
        private bool hasBones = false;
        private int numVertices = 0;
        private int numFaces = 0;
        private int numColorChannels = 0;
        private int numUVChannels = 0;

        private int vertexStride = 3 * sizeof(float);
        private int currentProgramID = -1;

        public GLMesh()
        {
        }

        public bool load(string filename, int flags = 0)
        {
            this.filename = filename;
            dirname = Path.GetDirectoryName(filename);

            // Create an instance of the Importer class
            AssimpContext importer = new AssimpContext();

            // Logging callback
            LogStream logstream = new LogStream(delegate(string msg, string userData)
            {
                Console.WriteLine(msg);
            });
            logstream.Attach();

            Scene scene = importer.ImportFile(filename, (PostProcessSteps)flags |
                PostProcessSteps.CalculateTangentSpace |
                PostProcessSteps.Triangulate |
                PostProcessSteps.JoinIdenticalVertices |
                PostProcessSteps.SortByPrimitiveType |
                PostProcessSteps.OptimizeGraph |
                PostProcessSteps.OptimizeMeshes );
            //const aiScene* scene = importer.ReadFile( filename, aiProcessPreset_TargetRealtime_MaxQuality);

            // If the import failed, stop
            if (scene == null)
            {
                return false;
            }

            numVertices = 0;
            numFaces = 0;

            for (int i = 0; i < scene.MeshCount; i++)
            {
                numVertices += scene.Meshes[i].VertexCount;
                numFaces += scene.Meshes[i].FaceCount;
            }
            // FIXME: What if different parts of model have different vertex channels available?
            hasBones = scene.Meshes[0].HasBones;
            hasNormals = scene.Meshes[0].HasNormals;
            numColorChannels = scene.Meshes[0].VertexColorChannelCount;
            numUVChannels = scene.Meshes[0].TextureCoordinateChannelCount;

            // Vertex stride = size of data for each vertice
            calculateVertexStride();

            // Allocate memory for keeping our VBO and IBO data
            vertexElementsData = new float[numVertices * vertexStride / sizeof(float)];
            facesData = new uint[numFaces * 3];

            // Prepare structures and data for rendering
            copyAiMeshes(scene);
            copyAiMaterials(scene);

            rootNode = new MeshNode();
            copyAiNodes(scene.RootNode, ref rootNode);
            copyAiAnimations(scene);
            // Upload data to GPU
            genVAO();

            return true;
        }
        public bool loadAnimation(string filename, string animationName, int flags = 0)
        {
            dirname = Path.GetDirectoryName(filename);

            // Create an instance of the Importer class
            AssimpContext importer = new AssimpContext();

            // Logging callback
            LogStream logstream = new LogStream(delegate(string msg, string userData)
            {
                Console.WriteLine(msg);
            });
            logstream.Attach();

            Scene scene = importer.ImportFile(filename,
                              PostProcessSteps.CalculateTangentSpace |
                              PostProcessSteps.Triangulate |
                              PostProcessSteps.JoinIdenticalVertices |
                              PostProcessSteps.SortByPrimitiveType |
                              PostProcessSteps.LimitBoneWeights);
                //PostProcessSteps.OptimizeGraph |
                //PostProcessSteps.OptimizeMeshes);
            //const aiScene* scene = importer.ReadFile( filename, aiProcessPreset_TargetRealtime_MaxQuality);

            // If the import failed, stop
            if (scene == null)
            {
                return false;
            }

            if (scene.AnimationCount > 0)
            {
                copyAiAnimation(scene.Animations[0], animationName);
                return true;
            }

            return false;
        }
        private void copyAiMaterials(Scene scene)
        {
            numMaterials = scene.MaterialCount;
            materials = new Material[numMaterials];

            for (int i = 0; i < numMaterials; i++)
            {
                var aimaterial = scene.Materials[i];

                int texIndex = 0;
                TextureSlot texture;

                // TODO: implement multiple diffuse textures for texture blending and GIA
                if (aimaterial.GetMaterialTexture(TextureType.Diffuse, texIndex, out texture))
                {
                    materials[i].textureDiffuse = Resources.getTexture(dirname + "/" + texture.FilePath);
                }
                // TODO: implement material -> shader mapping, probably using some configuration format
                materials[i].shader = Resources.getShader("shader/" + Resources.getBaseName(filename));
            }
        }
        private void copyAiMeshes(Scene scene)
        {
            numMeshParts = scene.MeshCount;
            meshParts = new MeshPart[numMeshParts];

            for (int i = 0; i < numMeshParts; i++)
            {
                //var meshPart = meshParts[i];
                var aimesh = scene.Meshes[i];

                meshParts[i].indexMaterial = aimesh.MaterialIndex;
                meshParts[i].numFaces = aimesh.FaceCount;
                meshParts[i].numVertices = aimesh.VertexCount;
                meshParts[i].indexVertices = indexVBO;
                meshParts[i].indexFaces = indexIBO;
                meshParts[i].baseVertex = (meshParts[i].indexVertices * sizeof(float) / (vertexStride));

                for (int n = 0; n < meshParts[i].numVertices; n++)
                {
                    for (int v = 0; v < 3; v++)
                    {
                        vertexElementsData[indexVBO++] = aimesh.Vertices[n][v];
                    }

                    if (hasNormals)
                    {
                        for (int v = 0; v < 3; v++)
                        {
                            vertexElementsData[indexVBO++] = aimesh.Normals[n][v];
                        }
                    }

                    for (int channel = 0; channel < numColorChannels; channel++)
                    {
                        for (int v = 0; v < 4; v++)
                        {
                            vertexElementsData[indexVBO++] = aimesh.VertexColorChannels[channel][n][v];
                        }
                    }

                    for (int channel = 0; channel < numUVChannels; channel++)
                    {
                        for (int v = 0; v < 2; v++)
                        {
                            vertexElementsData[indexVBO++] = aimesh.TextureCoordinateChannels[channel][n][v];
                        }
                    }

                    if (hasBones)
                    {
                        // Reserve space for bone IDs and weights
                        unsafe
                        {
                            fixed(float *boneDataPtr = &vertexElementsData[indexVBO])
                            {
                                var boneDataVBO = (BoneData*)boneDataPtr;
                                // Reserve space for bone IDs
                                for (int bone = 0; bone < MAX_BONE_WEIGHTS; bone++)
                                {
                                    boneDataVBO->boneIDs[bone] = 0;
                                    boneDataVBO->boneWeights[bone] = 0.0f;
                                }
                            }
                            indexVBO += sizeof(BoneData) / sizeof(float);
                        }
                    }
                }

                int baseVertex = (meshParts[i].indexVertices * sizeof(float) / (vertexStride));
                for (int n = 0; n < meshParts[i].numFaces; n++)
                {
                    for (int v = 0; v < 3; v++)
                    {
                        facesData[indexIBO++] = (uint)(aimesh.Faces[n].Indices[v] + baseVertex);
                    }
                }

                if (hasBones)
                    copyAiBones(meshParts[i], aimesh);
            }
        }
        private void copyAiNodes(Node node, ref MeshNode targetNode)
        {
            targetNode.name = node.Name;
            copyAiMat(node.Transform, ref targetNode.transformation);
            if (node.MeshCount > 0)
            {
                targetNode.numMeshes = node.MeshCount;
                targetNode.meshes = new int[targetNode.numMeshes];

                for (int i = 0; i < targetNode.numMeshes; i++)
                {
                    targetNode.meshes[i] = node.MeshIndices[i];
                }
            }
            if (node.ChildCount > 0)
            {
                targetNode.numChildren = node.ChildCount;
                targetNode.children = new MeshNode[targetNode.numChildren];

                for (int i = 0; i < targetNode.numChildren; i++)
                {
                    copyAiNodes(node.Children[i], ref (targetNode.children[i]));
                }
            }
        }



        public void render(RenderParams renderParams)
        {
            GL.BindVertexArray(vertexArrayObject);
            GL.BindBuffer(BufferTarget.ArrayBuffer, vertexElementsBuffer);
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, facesBuffer);

            currentProgramID = -1;
            render(renderParams, rootNode);
            GL.BindVertexArray(0);
        }

        public void render(RenderParams renderParams, MeshNode node)
        {
            var parentModelMatrix = renderParams.matrices["modelMatrix"];
            renderParams.matrices["modelMatrix"] *= node.transformation;

            // Draw all meshes assigned to this node
            for (int i = 0; i < node.numMeshes; i++)
            {
                var meshPart = meshParts[node.meshes[i]];
                var material = materials[meshPart.indexMaterial];
                var shader = material.shader;

                if (material.textureDiffuse != null)
                {
                    renderParams.samplers["diffuseMap"] = 0;
                    material.textureDiffuse.bind();
                }

                if (shader.getProgramID() != currentProgramID)
                {
                    shader.use();
                    shader.setUniforms(renderParams);
                    currentProgramID = shader.getProgramID();
                }
                else
                {
                    shader.setUniform("modelMatrix", renderParams.matrices["modelMatrix"]);
                }

                GL.DrawElements(OpenTK.Graphics.OpenGL.PrimitiveType.Triangles, meshPart.numFaces * 3,
                    DrawElementsType.UnsignedInt, (meshPart.indexFaces * sizeof(uint)));
            }

            // Draw all children
            for (int i = 0; i < node.numChildren; i++)
            {
                render(renderParams, node.children[i]);
            }

            renderParams.matrices["modelMatrix"] = parentModelMatrix;
        }

        private void copyAiBones(MeshPart meshPart, Assimp.Mesh aimesh)
        {
            for (int i = 0; i < aimesh.BoneCount; i++)
            {
                int boneIndex = 0;
                string boneName = aimesh.Bones[i].Name;

                if (boneMapping.ContainsKey(boneName))
                {
                    boneIndex = boneMapping[boneName];
                }
                else
                {
                    boneIndex = numBones++;
                    BoneInfo bone = new BoneInfo();
                    copyAiMat(aimesh.Bones[i].OffsetMatrix, ref bone.offsetMatrix);
                    bones.Add(bone);

                    boneMapping[boneName] = boneIndex;
                }

                for (int j = 0; j < aimesh.Bones[i].VertexWeightCount; j++)
                {
                    int vertexID = aimesh.Bones[i].VertexWeights[j].VertexID;
                    float weight = aimesh.Bones[i].VertexWeights[j].Weight;
                    addBoneDataToVBO(meshPart, vertexID, boneIndex, weight);
                }
            }
        }

        private void copyAiAnimations(Scene scene)
        {
            for (int i = 0; i < scene.AnimationCount; i++)
            {
                var animation = scene.Animations[i];
                copyAiAnimation(animation, animation.Name);
            }
        }

        private void copyAiAnimation(Assimp.Animation animation, string animationName)
	    {
            var targetAnimation = new Animation();
		    targetAnimation.duration = (int)animation.DurationInTicks;
		    targetAnimation.ticksPerSecond = (int)animation.TicksPerSecond;
            targetAnimation.nodes = new Dictionary<string, AnimationNode>();

		    for (int j = 0; j < animation.NodeAnimationChannelCount; j++)
		    {
			    var aichannel = animation.NodeAnimationChannels[j];
                var node = new AnimationNode();

			    node.keyFramePerTick = ((animation.DurationInTicks + 1) == aichannel.PositionKeyCount &&
                    aichannel.PositionKeyCount == aichannel.ScalingKeyCount &&
                    aichannel.ScalingKeyCount == aichannel.RotationKeyCount);

                node.positionKeys = new List<AnimationKey>();
			    node.positionKeys.Capacity = aichannel.PositionKeyCount;
                node.scalingKeys = new List<AnimationKey>();
			    node.scalingKeys.Capacity = aichannel.ScalingKeyCount;
                node.rotationKeys = new List<AnimationRotationKey>();
			    node.rotationKeys.Capacity = aichannel.RotationKeyCount;

			    foreach (var aikey in aichannel.PositionKeys)
			    {
                    var key = new AnimationKey();
                    key.time = (int)aikey.Time;
				    key.value = new Vector3(aikey.Value.X, aikey.Value.Y, aikey.Value.Z);
                    node.positionKeys.Add(key);
			    }

                foreach (var aikey in aichannel.ScalingKeys)
                {
                    var key = new AnimationKey();
				    key.time = (int)aikey.Time;
                    key.value = new Vector3(aikey.Value.X, aikey.Value.Y, aikey.Value.Z);
                    node.scalingKeys.Add(key);
                }

                foreach (var aikey in aichannel.RotationKeys)
                {
                    var key = new AnimationRotationKey();
				    key.time = (int)aikey.Time;
				    //key.value = new OpenTK.Quaternion(aikey.Value.W, aikey.Value.X, aikey.Value.Y, aikey.Value.Z);
                    key.value = new OpenTK.Quaternion(aikey.Value.X, aikey.Value.Y, aikey.Value.Z, aikey.Value.W);
                    node.rotationKeys.Add(key);
			    }

                targetAnimation.nodes[aichannel.NodeName] = node;
		    }

            animations[animationName] = targetAnimation;
	    }

        private void addBoneDataToVBO(MeshPart meshPart, int vertexID, int boneID, float weight)
	    {
		    int boneDataIndexVBO = (vertexID + meshPart.baseVertex + 1) * (vertexStride / sizeof(float)) - MAX_BONE_WEIGHTS * 2;

            unsafe
            {
                fixed(float *boneDataPtr = &vertexElementsData[boneDataIndexVBO])
                {
                    var boneDataVBO = (BoneData*)boneDataPtr;
                    for (int i = 0; i < MAX_BONE_WEIGHTS; i++)
                    {
                        if (boneDataVBO->boneWeights[i] == 0.0f)
                        {
                            boneDataVBO->boneIDs[i] = boneID;
                            boneDataVBO->boneWeights[i] = weight;
                            return;
                        }
                    }
                }
            }

		    // Not supposed to happen
		    Console.Write("[Mesh] Run out of available bone weights!");
	    }

        private void calculateVertexStride()
        {
            vertexStride = 3 * sizeof(float); // position (x, y, z)

            if (hasNormals)
            {
                vertexStride += 3 * sizeof(float); // normals (x, y, z)
            }

            for (int i = 0; i < numColorChannels; i++)
            {
                vertexStride += 4 * sizeof(float); // colors (r, g, b, a)
            }

            for (int i = 0; i < numUVChannels; i++)
            {
                vertexStride += 2 * sizeof(float); // texture coordinates (u, v)
            }

            if (hasBones)
            {
                // Bone IDs and weights
                //vertexStride += sizeof(BoneData);
                vertexStride += 4 * sizeof(float) + 4 * sizeof(int);
            }
        }
        private void genVAO()
        {
            vertexArrayObject = GL.GenVertexArray();
            GL.BindVertexArray(vertexArrayObject);

            // Buffer for vertex elements
            vertexElementsBuffer = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ArrayBuffer, vertexElementsBuffer);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(vertexStride * numVertices), vertexElementsData, BufferUsageHint.StaticDraw);

            int attrib = 0;
            int offset = 0;

            GL.EnableVertexAttribArray(attrib);
            //C++ TO C# CONVERTER TODO TASK: There is no equivalent to 'reinterpret_cast' in C#:
            GL.VertexAttribPointer(attrib++, 3, VertexAttribPointerType.Float, false, vertexStride, offset);
            offset += 3 * sizeof(float);

            if (hasNormals)
            {
                GL.EnableVertexAttribArray(attrib);
                GL.VertexAttribPointer(attrib++, 3, VertexAttribPointerType.Float, false, vertexStride, offset);
                offset += 3 * sizeof(float);
            }

            for (int i = 0; i < numColorChannels; i++)
            {
                GL.EnableVertexAttribArray(attrib);
                GL.VertexAttribPointer(attrib++, 4, VertexAttribPointerType.Float, false, vertexStride, offset);
                offset += 4 * sizeof(float);
            }

            for (int i = 0; i < numUVChannels; i++)
            {
                GL.EnableVertexAttribArray(attrib);
                GL.VertexAttribPointer(attrib++, 2, VertexAttribPointerType.Float, false, vertexStride, offset);
                offset += 2 * sizeof(float);
            }

            if (hasBones)
            {
                // Bone IDs
                GL.EnableVertexAttribArray(attrib);
                GL.VertexAttribIPointer(attrib++, 4, VertexAttribIntegerType.Int, vertexStride, (IntPtr)offset);
                offset += 4 * sizeof(Int32);
                // Bone weights
                GL.EnableVertexAttribArray(attrib);
                GL.VertexAttribPointer(attrib++, 4, VertexAttribPointerType.Float, false, vertexStride, offset);
                offset += 4 * sizeof(float);
            }

            // Buffer for faces
            facesBuffer = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, facesBuffer);
            GL.BufferData(BufferTarget.ElementArrayBuffer, (IntPtr)(sizeof(uint) * numFaces * 3), facesData,
                BufferUsageHint.StaticDraw);

            // Unbind buffers
            GL.BindVertexArray(0);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);
        }

        public static void copyAiMat(Assimp.Matrix4x4 from, ref Matrix4 to)
        {
            to.M11 = from.A1;
            to.M21 = from.A2;
            to.M31 = from.A3;
            to.M41 = from.A4;
            to.M12 = from.B1;
            to.M22 = from.B2;
            to.M32 = from.B3;
            to.M42 = from.B4;
            to.M13 = from.C1;
            to.M23 = from.C2;
            to.M33 = from.C3;
            to.M43 = from.C4;
            to.M14 = from.D1;
            to.M24 = from.D2;
            to.M34 = from.D3;
            to.M44 = from.D4;
        }
    }
}