﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

namespace ChaoGarden.GLGraphics
{
    public class GLGraphics
    {
        public GLGraphics()
        {

        }
        public void Dispose()
        {

        }
        public void setup_context(int width, int height)
        {
            this.width = width;
            this.height = height;
            GL.Enable(EnableCap.DepthTest);
            //   GL.Enable(GL_CULL_FACE);
            //   GL.CullFace(GL_FRONT);
            GL.DepthFunc(DepthFunction.Less);
            GL.ClearColor(0.0f, 0.0f, 0.0f, 1.0f);
            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
        }
        public void render()
        {
            clear();
        }
        public void clear()
        {
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
        }
        public void clearColor()
        {
            GL.Clear(ClearBufferMask.ColorBufferBit);
        }
        public void clearDepth()
        {
            GL.Clear(ClearBufferMask.DepthBufferBit);
        }
        public float aspectRatio()
        {
            return (float)width / height;
        }

        public int width;
        public int height;
    }
}
