﻿using System;
using System.Collections.Generic;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using System.IO;

namespace ChaoGarden.GLGraphics
{
    public class GLShader : IResourceInterface
    {
        private int programID;
        private string filename;
        private string vertexShaderCode;
        private string fragmentShaderCode;

        public GLShader()
        {
        }
        public void Dispose()
        {
        }
        public bool load(string filename)
        {
            this.filename = filename;
            vertexShaderCode = getCodeFromFile(filename + ".vert");
            fragmentShaderCode = getCodeFromFile(filename + ".frag");
            compileShader();
            return true;
        }
        public void compileShader()
        {
            var vertexShaderID = GL.CreateShader(ShaderType.VertexShader);
            var fragmentShaderID = GL.CreateShader(ShaderType.FragmentShader);

            // Compile Vertex Shader
            GL.ShaderSource(vertexShaderID, vertexShaderCode);
            GL.CompileShader(vertexShaderID);
            checkShader(vertexShaderID);

            // Compile Fragment Shader
            GL.ShaderSource(fragmentShaderID, fragmentShaderCode);
            GL.CompileShader(fragmentShaderID);
            checkShader(fragmentShaderID);

            // Link the program
            programID = GL.CreateProgram();
            GL.AttachShader(programID, vertexShaderID);
            GL.AttachShader(programID, fragmentShaderID);
            GL.LinkProgram(programID);
            Console.Write(programID);

            GL.DeleteShader(vertexShaderID);
            GL.DeleteShader(fragmentShaderID);
        }
        public void use()
        {
            GL.UseProgram(programID);
        }
        public int getProgramID()
        {
            return programID;
        }


        // Setting floats
        public void setUniform(string name, float value)
        {
            int loc = GL.GetUniformLocation(programID, name);
            GL.Uniform1(loc, 1, ref value);
        }

        // Setting vectors
        public void setUniform(string name, Vector2 vector)
        {
            int loc = GL.GetUniformLocation(programID, name);
            GL.Uniform2(loc, vector);
        }
        public void setUniform(string name, Vector3 vector)
        {
            int loc = GL.GetUniformLocation(programID, name);
            GL.Uniform3(loc, vector);
        }
        public void setUniform(string name, Vector4 vector)
        {
            int loc = GL.GetUniformLocation(programID, name);
            GL.Uniform4(loc, vector);
        }

        // Setting 3x3 matrices
        public void setUniform(string name, Matrix3 matrix)
        {
            int loc = GL.GetUniformLocation(programID, name);
            GL.UniformMatrix3(loc, false, ref matrix);
        }

        // Setting 4x4 matrices
        public void setUniform(string name, Matrix4 matrix)
        {
            int loc = GL.GetUniformLocation(programID, name);
            GL.UniformMatrix4(loc, false, ref matrix);
        }
        public void setUniform(string name, Matrix4[] matrix)
        {
            int loc = GL.GetUniformLocation(programID, name);
            unsafe
            {
                fixed (Matrix4* matrixPointer = &matrix[0])
                {
                    GL.UniformMatrix4(loc, matrix.Length, false, (float*)matrixPointer);
                }
            }
        }

        // Setting integers
        public void setUniform(string name, int value)
        {
            int loc = GL.GetUniformLocation(programID, name);
            GL.Uniform1(loc, value);
        }
        public void setModelAndNormalMatrix(string modelMatrixName, string normalMatrixName, Matrix4 modelMatrix)
        {
            setUniform(modelMatrixName, modelMatrix);
            setUniform(normalMatrixName, Matrix4.Transpose(modelMatrix.Inverted()));
        }

        public void setUniforms(RenderParams renderParams)
        {
            foreach (var uniform in renderParams.matrices)
            {
                setUniform(uniform.Key, uniform.Value);
            }
            foreach (var uniform in renderParams.matrixArrays)
            {
                setUniform(uniform.Key, uniform.Value);
            }
            foreach (var uniform in renderParams.samplers)
            {
                setUniform(uniform.Key, uniform.Value);
            }
        }

        private string getCodeFromFile(string filename)
	    {
            string shaderCode = "";
            try
            {
                using (StreamReader reader = new StreamReader(filename))
                {
                    shaderCode = reader.ReadToEnd();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("[Shader] Can't read " + filename);
                Console.WriteLine(e.Message);
            }
		    return shaderCode;
	    }

        private void checkShader(int shaderID)
        {
            int result = 0;
            GL.GetShader(shaderID, ShaderParameter.CompileStatus, out result);
            string errorMessage;
            GL.GetShaderInfoLog(shaderID, out errorMessage);
            // One byte long log contains just string termination byte
            if (errorMessage.Length > 1)
            {
                Console.Write("[Shader] ");
                Console.Write(filename);
                Console.Write(" ");
                Console.WriteLine(errorMessage);
            }
        }
    }
}