﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using System;
using System.Collections.Generic;

namespace ChaoGarden.GLGraphics
{
    public class RenderParams
    {
        public Dictionary<string, Matrix4> matrices = new Dictionary<string, Matrix4>();
        public Dictionary<string, Matrix4[]> matrixArrays = new Dictionary<string, Matrix4[]>();
        public Dictionary<string, int> samplers = new Dictionary<string, int>();
        public Dictionary<string, Vector4> vectors = new Dictionary<string, Vector4>();
    }

    public class GLWindow : GameWindow
    {
        private GameFlowController controller;
        private int width;
        private int height;

        private double currentTime = 0.0;
        private double frameCounterResetTime = 0.0;
        private int framesCount = 0;

        public GLWindow(int width, int height) :
            base(width, height, new GraphicsMode(), "Chao Garden", GameWindowFlags.Default,
            DisplayDevice.Default, 3, 3, GraphicsContextFlags.Default)
        {
            this.width = width;
            this.height = height;
        }
        protected override void OnLoad(EventArgs e)
        {
            controller = new GameFlowController();
            controller.engine.graphics.setup_context(width, height);
        }

        protected override void OnRenderFrame(FrameEventArgs e)
        {
            /* Render here */
            controller.render();
            SwapBuffers();
        }

        protected override void OnUpdateFrame(FrameEventArgs e)
        {
            /* Calculate time passed since previous frame */
            currentTime += e.Time;
            float deltaTime = (float)e.Time;

            /* Measure speed */
            framesCount++;
            if (currentTime - frameCounterResetTime >= 1.0)
            {
                Console.Write(framesCount + " frames/second, " +
                    (1000.0/(double)(framesCount)) + " ms/frame\n");
                framesCount = 0;
                frameCounterResetTime += 1.0;
            }
            controller.update(deltaTime);
        }
    }
}