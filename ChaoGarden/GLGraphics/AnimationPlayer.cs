﻿using OpenTK;
using System.Collections.Generic;

namespace ChaoGarden.GLGraphics
{
    public class AnimationPlayer
    {
        public const int MAX_BONES = 110;
        private GLMesh mesh;
        private string animationName;
        private float animationTick = 0F;
        public Matrix4[] boneTransforms;
        private bool interpolateAnimations = true;

        public AnimationPlayer(GLMesh mesh)
        {
            this.mesh = mesh;
            // Fill in default bone transforms
            boneTransforms = new Matrix4[mesh.numBones];
            for (int i = 0; i < boneTransforms.Length; i++)
            {
                boneTransforms[i] = Matrix4.Identity;
            }
        }

        public void playAnimation(float deltaTime)
        {
            if (mesh.animations.ContainsKey(animationName))
            {
                var animation = mesh.animations[animationName];
                animationTick += deltaTime * animation.ticksPerSecond;

                while (animationTick > animation.duration)
                {
                    animationTick -= animation.duration;
                }

                transformBones(animation, mesh.rootNode, OpenTK.Matrix4.Identity);
            }
        }
        public void startAnimation(string animationName)
        {
            this.animationName = animationName;
        }

        private void transformBones(Animation animation, MeshNode node, Matrix4 parentTransform)
        {
            var nodeTransformation = node.transformation;
            if (animation.nodes.ContainsKey(node.name))
            {
                var animNode = animation.nodes[node.name];

                if (interpolateAnimations)
                {
                    var position = calcInterpolatedKey(animNode.positionKeys);
                    var scale = calcInterpolatedKey(animNode.scalingKeys);
                    var rotation = calcInterpolatedKey(animNode.rotationKeys);

                    nodeTransformation = Matrix4.CreateScale(scale) *
                        Matrix4.CreateFromQuaternion(rotation) *
                        Matrix4.CreateTranslation(position);
                }
                else
                {
                    int positionKeyIndex;
                    int scalingKeyIndex;
                    int rotationKeyIndex;
                    if (animNode.keyFramePerTick)
                    {
                        positionKeyIndex = scalingKeyIndex = rotationKeyIndex = (int)animationTick;
                    }
                    else
                    {
                        positionKeyIndex = findKey(animNode.positionKeys);
                        scalingKeyIndex = findKey(animNode.scalingKeys);
                        rotationKeyIndex = findKey(animNode.rotationKeys);
                    }

                    var position = animNode.positionKeys[positionKeyIndex].value;
                    var scale = animNode.scalingKeys[scalingKeyIndex].value;
                    var rotation = animNode.rotationKeys[rotationKeyIndex].value;

                    nodeTransformation = Matrix4.CreateScale(scale) *
                        Matrix4.CreateFromQuaternion(rotation) *
                        Matrix4.CreateTranslation(position);
                }
            }
            var globalTransformation = nodeTransformation * parentTransform;

            var boneMapping = mesh.boneMapping;
            if (boneMapping.ContainsKey(node.name))
            {
                int boneIndex = boneMapping[node.name];
                boneTransforms[boneIndex] = mesh.bones[boneIndex].offsetMatrix * globalTransformation;
            }

            for (int i = 0; i < node.numChildren; i++)
            {
                transformBones(animation, node.children[i], globalTransformation);
            }
        }

        private int findKey(List<AnimationKey> keys)
        {
            int numKeys = keys.Count - 1;
            for (int i = 0; i < numKeys; i++)
            {
                if (animationTick < (float)keys[i + 1].time)
                {
                    return i;
                }
            }
            return 0;
        }
        private int findKey(List<AnimationRotationKey> keys)
        {
            int numKeys = keys.Count - 1;
            for (int i = 0; i < numKeys; i++)
            {
                if (animationTick < (float)keys[i + 1].time)
                {
                    return i;
                }
            }
            return 0;
        }
        private Quaternion calcInterpolatedKey(List<AnimationRotationKey> keys)
        {
            if (keys.Count == 1)
            {
                return keys[0].value;
            }

            int keyIndex = findKey(keys);
            int nextKeyIndex = (keyIndex + 1);
            var key = keys[keyIndex];
            var nextKey = keys[nextKeyIndex];

            int deltaTime = nextKey.time - key.time;
            float factor = (animationTick - (float)key.time) / deltaTime;

            return Quaternion.Slerp(key.value, nextKey.value, factor);
        }
        private Vector3 calcInterpolatedKey(List<AnimationKey> keys)
        {
            if (keys.Count == 1)
            {
                return keys[0].value;
            }

            int keyIndex = findKey(keys);
            int nextKeyIndex = (keyIndex + 1);
            var key = keys[keyIndex];
            var nextKey = keys[nextKeyIndex];

            int deltaTime = nextKey.time - key.time;
            float factor = (animationTick - (float)key.time) / deltaTime;

            return Vector3.Lerp(key.value, nextKey.value, factor);
        }
    }
}
