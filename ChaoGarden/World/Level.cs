﻿using ChaoGarden.GLGraphics;
using OpenTK;
using System.Collections.Generic;

namespace ChaoGarden.World
{
    public class Level : Entity
    {
        private GLMesh mesh;
        public Level()
        {
            mesh = Resources.getModel("level/chaostgneut/chaostgneut.dae");
            //mesh = Resources::getModel("level/triangle.nff");
        }
        public override void render(RenderParams renderParams)
        {
            renderParams.matrices["modelMatrix"] = Matrix4.Identity;
            mesh.render(renderParams);
        }
    }
}