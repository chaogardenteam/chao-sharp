﻿using System.Collections.Generic;
using ChaoGarden.GLGraphics;

namespace ChaoGarden.World
{
    public class World
    {
        private List<Entity> entities = new List<Entity>();
        private Camera camera;

        public World()
        {

        }
        public void add(Entity entity)
        {
            entities.Add(entity);
        }
        public void setCamera(Camera newCamera)
        {
            camera = newCamera;
        }
        public void render()
        {
            RenderParams renderParams = new RenderParams();
            renderParams.matrices["projMatrix"] = camera.getProjectionMatrix();
            renderParams.matrices["viewMatrix"] = camera.getViewMatrix();

            foreach (var entity in entities)
            {
                entity.render(renderParams);
            }
        }

        public void update(float deltaTime)
        {
            foreach (var entity in entities)
            {
                entity.update(deltaTime);
            }
        }
    }
}
