﻿using OpenTK;
using System.Collections.Generic;

namespace ChaoGarden.World
{
    public class Camera : Entity
    {
        public Vector3 upVector = new Vector3(0, 1, 0);
        private Vector3 center;

        public Camera()
	    {
		    position = new Vector3(79.49942511343389f, 61.59999991099296f, 77.14982550220141f);
		    center = new Vector3(56.872008115464226f, 51.59999991099672f, 54.52240850423203f);
	    }

        public Matrix4 getProjectionMatrix()
        {
            return Matrix4.CreatePerspectiveFieldOfView(MathHelper.DegreesToRadians(58), 4.0f / 3.0f, 0.1f, 1000.0f); // Far clipping plane -  Near clipping plane -  Aspect Ratio -  The horizontal Field of View
        }
        public Matrix4 getViewMatrix()
        {
            return Matrix4.LookAt(position, center, upVector);
        }
    }
}