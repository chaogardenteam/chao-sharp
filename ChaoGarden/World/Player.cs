﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChaoGarden;
using ChaoGarden.GLGraphics;
using OpenTK;

namespace ChaoGarden.World
{
    class Player : Entity
    {
        private GLMesh mesh;
        private AnimationPlayer animationPlayer;

        public Player()
        {
            mesh = Resources.getModel("model/Sonic/Sonic.fbx", (int)Assimp.PostProcessSteps.FlipUVs);
            //mesh.loadAnimation("model/Sonic/sn_walk_loop.fbx", "sn_walk_loop");
            mesh.loadAnimation("model/Sonic/sn_idle00.fbx", "sn_idle00");

            animationPlayer = new AnimationPlayer(mesh);
            animationPlayer.startAnimation("sn_idle00");
        }


        public override void render(RenderParams renderParams)
        {
            renderParams.matrices["modelMatrix"] = Matrix4.CreateScale(new Vector3(20, 20, 20));
            renderParams.matrices["modelMatrix"] *= Matrix4.CreateTranslation(new Vector3(22.93f, 10f, 14.30883f));
            renderParams.matrixArrays["bones"] = animationPlayer.boneTransforms;

            mesh.render(renderParams);
        }

        public override void update(float deltaTime)
        {
            animationPlayer.playAnimation(deltaTime);
        }

    }
}
