﻿using ChaoGarden.GLGraphics;
using OpenTK;
using System.Collections.Generic;

namespace ChaoGarden.World
{
    public class Entity
    {
        protected Vector3 position = new Vector3(0, 0, 0);
        protected Vector3 rotation = new Vector3(0, 0, 0);
        protected Vector3 scale = new Vector3(1, 1, 1);

	    public Entity()
	    {

	    }
	    public virtual void render(RenderParams renderParams)
	    {
	    }
        public virtual void update(float deltaTime)
        {
        }
    }
}
