﻿using System.Collections.Generic;
using ChaoGarden.GLGraphics;

namespace ChaoGarden
{
    public class Engine
    {
        public Engine()
        {
            /* Init graphics engine */
            graphics = new GLGraphics.GLGraphics();
            world = new World.World();
        }

        public GLGraphics.GLGraphics graphics;
        public World.World world;
    }
}