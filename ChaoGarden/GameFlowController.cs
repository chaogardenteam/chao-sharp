﻿using ChaoGarden.World;
using System.Collections.Generic;

namespace ChaoGarden
{
    public class GameFlowController
    {
        public GameFlowController()
        {
            engine = new Engine();
            engine.world.setCamera(new Camera());

            var level = new Level();
            engine.world.add(level);

            var player = new Player();
            engine.world.add(player);
        }

        public Engine engine = null;

        public void render()
        {
            engine.graphics.clear();
            engine.world.render();
        }

        public void update(float deltaTime)
        {
            engine.world.update(deltaTime);
        }
    }
}