﻿using System.Collections.Generic;
using System;
using ChaoGarden.GLGraphics;

namespace ChaoGarden
{
    public interface IResourceInterface
    { 
        bool load(string filename);
    }
    public interface IResourceFlagsInterface
    {
        bool load(string filename, int flags);
    }

    public class Resources
    {
        private Dictionary<string, WeakReference<GLMesh>> models = new Dictionary<string, WeakReference<GLMesh>>();
        private Dictionary<string, WeakReference<GLShader>> shaders = new Dictionary<string, WeakReference<GLShader>>();
        private Dictionary<string, WeakReference<GLTexture>> textures = new Dictionary<string, WeakReference<GLTexture>>();
        public Resources()
        {
        }
        private static Resources instance = new Resources();
        public static Resources getInstance()
        {
            return instance;
        }

        public static T get<T>(Dictionary<string, WeakReference<T>> items, string filename)
            where T : class, IResourceInterface, new()
	    {
            T item;
		    if (items.ContainsKey(filename))
		    {
                items[filename].TryGetTarget(out item);
			    if (item != null)
			    {
				    return item;
			    }
		    }

		    item = new T();
		    items[filename] = new WeakReference<T>(item);

            if (Environment.OSVersion.Platform == PlatformID.Win32NT)
            {
                var filenameWin = filename.Replace('/', '\\');
		        item.load(filenameWin);
            }
            else
		        item.load(filename);
		    return item;
	    }

        public static T get<T>(Dictionary<string, WeakReference<T>> items, string filename, int flags = 0)
            where T : class, IResourceFlagsInterface, new()
        {
            T item;
            if (items.ContainsKey(filename))
            {
                items[filename].TryGetTarget(out item);
                if (item != null)
                {
                    return item;
                }
            }

            item = new T();
            items[filename] = new WeakReference<T>(item);

            if (Environment.OSVersion.Platform == PlatformID.Win32NT)
            {
                var filenameWin = filename.Replace('/', '\\');
                item.load(filenameWin, flags);
            }
            else
                item.load(filename, flags);
            return item;
        }

        public static GLMesh getModel(string filename, int flags = 0)
        {
            var instance = getInstance();
            return Resources.get<GLMesh>(instance.models, filename, flags);
        }
        public static GLShader getShader(string filename)
        {
            var instance = getInstance();
            return Resources.get<GLShader>(instance.shaders, filename);
        }
        public static GLTexture getTexture(string filename)
        {
            var instance = getInstance();
            return Resources.get<GLTexture>(instance.textures, filename);
        }

        public static string getBaseName(string filename)
        {
            int separator;
            if (Environment.OSVersion.Platform == PlatformID.Win32NT)
                separator = filename.LastIndexOf("/\\");
            else
                separator = filename.LastIndexOf("/");
            int ext = filename.LastIndexOf(".");

            if (separator > 0)
            {
                separator += 1;
                if (ext > separator)
                    return filename.Substring(separator, ext - separator);
                else
                    return filename.Substring(separator);
            }
            else
                return filename;
         }
    }
}