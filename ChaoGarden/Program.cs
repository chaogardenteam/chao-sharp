﻿using ChaoGarden.GLGraphics;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChaoGarden
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            using (var game = new GLWindow(800, 600))
            {
                game.VSync = VSyncMode.Off;
                game.Run(0);
            }
        }
    }
}
